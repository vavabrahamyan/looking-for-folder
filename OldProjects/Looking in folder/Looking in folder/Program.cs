﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Looking_in_folder
{
    class Program
    {
        static void Main(string[] args)
        {
            string adress = @"C:\Users\user\Desktop\Directories";
            string fileName = "Vavs";
            Looking_In_Folder.SearchFileInFolder(adress, fileName);
            Console.ReadLine();
        }
    }

    static class Looking_In_Folder
    {
        //Search file in Folders and subfolders
        public static bool IsEmpty { get; set; } = false;

        private static bool IsEmptyFolder( string adress)
        {
            if (System.IO.Directory.GetDirectories(adress).Length +
                System.IO.Directory.GetFiles(adress).Length > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private static string[] SubFolderSearch( string adress)
        {
            var Directories = new DirectoryInfo(adress).GetDirectories("*.*", SearchOption.AllDirectories);
            string[] str = new string[Directories.Length + 1];
            for (int i = 0; i < Directories.Length ; i++)
            {
                str[i] = Directories[i].FullName;
            }
            return str;
        }

        private static bool IsFoundFile(string adress, string fileName)
        {
            bool FileFound = false;
            DirectoryInfo dir = new DirectoryInfo(adress);
            foreach (var item in dir.GetDirectories())
            {
               if(item.Name.ToUpper() == fileName.ToUpper())
                {
                    FileFound = true;
                }
                Console.WriteLine($"File is found  ---  {item.FullName}");
            }
            return FileFound;
        }

        public static void SearchFileInFolder(string adress, string fileName)
        {
            string[] adreses = null;
            if (!IsEmptyFolder(adress))
            {
                adreses = SubFolderSearch(adress);
            }

            for (int i = 0; i < adress.Length; i++)
            {
                IsFoundFile(adreses[i], fileName);
            }
        }


    }
}
