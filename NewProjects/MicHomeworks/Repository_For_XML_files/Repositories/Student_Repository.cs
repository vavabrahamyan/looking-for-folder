﻿using Repository_For_XML_files.Constants;
using Repository_For_XML_files.Models;
using System;
using System.Xml;

namespace Repository_For_XML_files.Repositories
{
    class Student_Repository:BaseRepository<Student>
    {
        public Student_Repository(string fileName) : base(fileName) { }

        protected override Student ToModel(XmlNode xnode)
        {
            var item = new Student();
            foreach (XmlNode xchild in xnode.ChildNodes)
            {
                switch (xchild.Name)
                {
                    case Keywords.FirstName:
                        item.FirstName = xchild.InnerText;
                        break;

                    case Keywords.LastName:
                        item.Lastname = xchild.InnerText;
                        break;

                    case Keywords.BirthDate:
                        if(DateTime.TryParse(xchild.InnerText, out DateTime date))
                        {
                            item.BirthDay = DateTime.Parse(xchild.InnerText);
                        }
                        break;
                }
            }
            return item;
        }
    }
}
